//
//  NovoContatoViewController.swift
//  Contato
//
//  Created by COTEMIG on 08/09/22.
//

import UIKit

protocol NovoContatoViewControllerDelegate{
    func salvarContato(contato : Contato)
}

class NovoContatoViewController: UIViewController {
    
    @IBOutlet weak var nomeField: UITextField!
    @IBOutlet weak var numeroField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var enderecoField: UITextField!
    public var delegate : NovoContatoViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func salvarContato(_ sender: Any) {
        let contato = Contato(nome: nomeField.text ?? "",
                              email: emailField.text ?? "",
                              endereco: enderecoField.text ?? "",
                              telefone: numeroField.text ?? "")
        delegate?.salvarContato(contato: contato)
        navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
